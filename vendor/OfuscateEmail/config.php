<?php

$ofuscateEmailConfig = 
[
    "reCaptcha" => [
        "recaptcha_site_key"        => "6LfUb-kUAAAAAAjWza_qQs9nf1AN1xdFjb3_xRHw",
        "recaptcha_secret_key"      => "6LfUb-kUAAAAAIBqKEhtQtZlrWMC4f8qzZvj2_dp",
        "min_score"                 => 1.5
    ],

    "routes" => [
        "url_handler_php_captcha"   => "vendor/OfuscateEmail/src/app/Http/handlerCapthcha.php",
        "images_url"                => "vendor/OfuscateEmail/src/public/assets/images/"
    ],

    "ofuscateValues" => [
        "data-true" =>[
            "email1"            => [
                "href"      => "mailto:correo1@deprueba.es",
                "text"      => "correo1@deprueba.es"          
            ],
            "email2"            => [
                "href"      => "mailto:correo2@deprueba.es",
                "text"      => "correo2@deprueba.es"          
            ],
            "phone1"            => [
                "html"      => "<a href='Tel:+34666662222'>+34666662222</a>"          
            ],
        ],
        "data-false" =>[
            "email1"    => [
                "image"             => "email1.png",
                "alt"               => "Imagen de email 1",
                "title"             => "Title email 1",
                "long_desc"         => "Email showed as an image to avoid being read from boots"
            ],
            "email2"    => [
                "html"              => "<img src='/vendor/OfuscateEmail/src/public/assets/images/email2.png'>"
            ]
        ],    
    ]
];
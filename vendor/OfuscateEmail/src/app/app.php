<?php


spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

include_once 'vendor/OfuscateEmail/config.php';

$urlHandler = $GLOBALS['ofuscateEmailConfig']['routes']['url_handler_php_captcha'];
$siteKey = $GLOBALS['ofuscateEmailConfig']['reCaptcha']['recaptcha_site_key'];

$imagesPath = $GLOBALS['ofuscateEmailConfig']['routes']['images_url'];
<?php

namespace app\Http;

include_once '../../../config.php';

class UseResponse {

    public static function verifyReCatpcha(array $post, array $server){
        //TODO refactor para poder llamar una sola vez a getCAptcha Values y obtener los dos valores pasando un array de keys
        $url ="https://www.google.com/recaptcha/api/siteverify";
        $data = [
            'secret' => $GLOBALS['ofuscateEmailConfig']['reCaptcha']['recaptcha_secret_key'],
            'response' => $post['token'],
            'remoteip' => $server['REMOTE_ADDR']
        ];
        $options = array(
            'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
            )
        );
        # Creates and returns stream context with options supplied in options preset 
        $context  = stream_context_create($options);
        # file_get_contents() is the preferred way to read the contents of a file into a string
        $response = file_get_contents($url, false, $context);
        # Takes a JSON encoded string and converts it into a PHP variable
        $res = json_decode($response, true);
        # END setting reCaptcha v3 validation data
        
        # Some developers want to
        # be able to control score result conditions, so I included that in this example.
        $min_score = $GLOBALS['ofuscateEmailConfig']['reCaptcha']['min_score'];
        if(!is_numeric($min_score))
           $min_score = 0;

        if ($res['success'] == true && $res['score'] >= $min_score) {
            $dataSuccess = $GLOBALS['ofuscateEmailConfig']['ofuscateValues']['data-true'];
            $response = [
                "success"   => true,
                "data"      => $dataSuccess
            ];   
        } 
        else {
            $dataError = $GLOBALS['ofuscateEmailConfig']['ofuscateValues']['data-false'];
            $response = [
                "success"   => false,
                "data"      => $dataError
            ];
        }
 
        return json_encode($response);
    }

}
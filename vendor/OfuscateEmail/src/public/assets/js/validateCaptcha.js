var siteKey = $('#site_key').val();
var handlerURL = "/" + $('#handler_url').val(); 
var imagesPath = "/"+ $("#images_path").val();

function done_success(response){
    var data  = response["data"];
    $("span[data-ofuscateEmail]").each(function(){
        var name = $(this).attr("data-ofuscateEmail");
        var htmlString = "";
        if(data[name] !== undefined){
            if(data[name]["html"] !== undefined){
                htmlString = data[name]["html"];
            }
            else{
                var hrefValue = data[name]["href"];
                var textValue = data[name]["text"];
                htmlString = "<a href='"+hrefValue+"'>"+textValue+"</a>";
            }

            $(this).append(htmlString);
            $(this).show();
            
        }
    });
}

function done_success_false(response){
    var data  = response["data"];
    $("span[data-ofuscateEmail]").each(function(){
        var name = $(this).attr("data-ofuscateEmail");

        if(data[name] !== undefined){
            if(data[name]["html"] !== undefined){
                htmlString = data[name]["html"];
            }
            else{
                var imageValue = data[name]["image"];
                var altValue = data[name]["alt"];
                var titleValue = data[name]["title"];
                var longDescValue = data[name]["long_desc"];
                htmlString = "<img src='"+imagesPath+imageValue+"' alt='" + altValue + "' title='" 
                                + titleValue + "' longdesc='" + longDescValue + "' >";
            }

            $(this).append(htmlString);
            $(this).show();           
        }
    });
}

function done_error(datos){
    console.log(datos.Message);
}

grecaptcha.ready(function() {
// do request for recaptcha token
// datos is promise with passed token
    grecaptcha.execute(siteKey)
        .then(function(token) {

        $.ajax({
            type: "POST",
            data: {
                token: token,
            },
            url: handlerURL,
            success: function(response) {
                response = JSON.parse(response);
                if (response['success'] == true) 
                    done_success(response);
                else
                    done_success_false(response);            
            },
            error: function(response){
                done_error(response);
            }
          });    
        })
});